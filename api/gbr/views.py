import os
import pickle
import numpy as np
import pandas as pd
from django.conf import settings
from rest_framework import views
from rest_framework import status
from rest_framework.response import Response

class Predict(views.APIView):
    def get(self, request):
        predictions = []

        with open('/home/florian/Documents/data_science/grb.pkl', 'rb') as file:
            model = pickle.load(file)
        try:
            reassignment_count = request.data['reassignment_count']
            reopen_count = request.data['reopen_count']
            sys_mod_count = request.data['sys_mod_count']
            made_sla = request.data['made_sla']
            contact_type = request.data['contact_type']
            location = request.data['location']
            category = request.data['category']
            subcategory = request.data['subcategory']
            u_symptom = request.data['u_symptom']
            impact = request.data['impact']
            urgency = request.data['urgency']
            priority = request.data['priority']
            knowledge = request.data['knowledge']
            u_priority_confirmation = request.data['u_priority_confirmation']
            notify = request.data['notify']
            closed_code = request.data['closed_code']

            feature = [
                reassignment_count, reopen_count, sys_mod_count, made_sla,
                contact_type, location, category, subcategory, u_symptom,
                impact, urgency, priority, knowledge, u_priority_confirmation,
                notify, closed_code
            ]
            
            result = model.predict(pd.DataFrame([feature]))
            predictions.append(result[0])

        except Exception as err:
            return Response(str(err), status=status.HTTP_400_BAD_REQUEST)

        return Response(predictions, status=status.HTTP_200_OK)